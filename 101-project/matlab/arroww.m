function X = arroww(A,B)
[n r] = size(B);
[maxx pos] = max(abs(A(1:n-1,n)));
A([pos n],:) = A([n pos],:);
A(:,[pos n]) = A(:,[n pos]);
B([pos n],:) = B([n pos],:);

for i = (1:n-1)
    A(n,n) = A(n,n) - A(i,n)*(A(n,i)/A(i,i));
    B(n,:) = B(n,:) - B(i,:)*(A(n,i)/A(i,i));
end

for k = 1:r
        B(n,k) = B(n,k)/A(n,n);
    for j = 1:(n-1)
        B(n-j,k) = (B(n-j,k) - A(n-j,n)*B(n,k))/A(n-j,n-j);
    end 
end
    B([pos n],:) = B([n pos],:);
    X = B;

end