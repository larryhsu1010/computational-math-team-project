function X = plu(A,B)
[n,r] = size(B);
P = 1:n;
      for k = 1:n-1
          [maxx,pos] = max(abs(A(k:n,k)));
          P(:,[k (pos + (k-1))]) = P(:,[(pos + (k-1)) k]);
          A([k (pos + (k-1))],:) = A([(pos + (k-1)) k],:);
		  
          for i = k+1:n
              A(i,k) = A(i,k)/A(k,k);
              j = k+1:n; 
	          A(i,j) = A(i,j) - A(i,k)*A(k,j);
          end
      end
      B(1:n,:) = B(P,:);
	  
	  for k = 1:r
          for i = 2:n
	          B(i,k) = B(i,k) - A(i,1:i-1)*B(1:i-1,k);
	      end
      end	  
	  
	  for k = 1:r
	      B(n,k) = B(n,k)/A(n,n);
          for i = 1:n-1
	          B(n-i,k) = (B(n-i,k) - A(n-i,n-i+1:n)*B(n-i+1:n,k))/(A(n-i,n-i));
	      end
      end	  
	  X = B;

end
