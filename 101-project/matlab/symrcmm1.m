function X = symrcmm1(A,B)

pp = symrcm(A);
[q t] = sort(pp);

A = A(pp,pp);
B = B(pp,:);

X = ppconjuu(A,B);

X = X(t,:);
