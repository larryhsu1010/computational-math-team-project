function X = pcg_test(A, B, err, stop)
pp = symrcm(A);
[q t] = sort(pp);

A = A(pp,pp);
B = B(pp,:);

X = ppconjuu(A,B, err, stop);

X = X(t,:);
end

function X = ppconjuu(A,B, err, stop)
X = B;
step = 0;
for k = 1:size(B,2)

     r=B(:,k)-A*X(:,k);
     z=(1./diag(A)).*r;
     p=z;
     rs=r'*r;

     while sqrt(rs)> err && step < stop 

              Ap=A*p;
              alpha=(p'*r)/(p'*Ap);
              X(:,k)=X(:,k)+alpha*p;
              r=r-alpha*Ap;
              z=(1./diag(A)).*r;
              
              beta=(Ap'*z)/(Ap'*p);
              p=z-beta*p;
              rs=r'*r;
              sqrt(rs)

          step = step +1
     end
end
end
