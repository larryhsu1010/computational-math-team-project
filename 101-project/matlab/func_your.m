function X = func_your(LSystem)

A = LSystem.A;
B = LSystem.B;
type = LSystem.type;
symm = LSystem.symm;
sprs = LSystem.sprs;
dd = LSystem.dd;

if type == 0
   if dd == 0
      if     sprs == 0                        
          if     symm == 0                    % 1,17
                 X = plu(A,B);
          elseif symm == 1   
                 [L d] = cholll(A);
		         if d == 1
		            X = spddd(L,B);
		         elseif d == 0
		            X = zeros(size(B));
		         end
          end
	  elseif sprs == 1
	         if     symm == 1                 % 8
                    X = ppconjuu(A,B);
	         elseif symm == 0                 
			        if  A == A'               % 11,12,13,14,15,16,18,19
					   
					   X = ppconjuu(A,B);
					   
					else
                       X = zeros(size(B));	  
                    end					   
             end					
	  end
   elseif dd == 1                             % 9,10
          X = ppconjuu(A,B);
   end

elseif type == 1                              % 2
    X = zeros(size(B));
elseif type == 2                              % 3,4
    X = utri(A,B);
elseif type == 3                              % 5
    X = trid(A,B);
elseif type == 4                              % 6
    X = ppconjuu(A,B);
elseif type == 5                              % 7
    X = arroww(A,B);
else
    X = zeros(size(B));
end

%=======================================================================================
function X = plu(A,B)
[n,r] = size(B);
P = 1:n;
      for k = 1:n-1
          [maxx,pos] = max(abs(A(k:n,k)));
          %t = P(k);
          %P(k) = P(pos + (k-1));
          %P(pos + (k-1)) = t;
          P(:,[k (pos + (k-1))]) = P(:,[(pos + (k-1)) k]);
          A([k (pos + (k-1))],:) = A([(pos + (k-1)) k],:);
		  
          for i = k+1:n
              A(i,k) = A(i,k)/A(k,k);
              j = k+1:n; 
	          A(i,j) = A(i,j) - A(i,k)*A(k,j);
          end
      end
      B(1:n,:) = B(P,:);
	  
	  for k = 1:r
          for i = 2:n
	          B(i,k) = B(i,k) - A(i,1:i-1)*B(1:i-1,k);
	      end
      end	  
	  
	  for k = 1:r
	      B(n,k) = B(n,k)/A(n,n);
          for i = 1:n-1
	          B(n-i,k) = (B(n-i,k) - A(n-i,n-i+1:n)*B(n-i+1:n,k))/(A(n-i,n-i));
	      end
      end	  
	  X = B;

end
%=======================================================================================
function [L d]= cholll(A)
[m n] = size(A);
d = 1;
for k = 1:n
    c1 = 0;
    for j = 1:(k-1)
    c1 = c1 + (A(k,j))^2;     % L(k+((j-1)*(2*n-j))/2) = L(k,j)
    end
    c1 = A(k,k)-c1;
    if c1 >= 0
    A(k,k) = c1^(1/2);        % L((((2*n-k+2)*(k-1))/2)+1) = L(k,k)
    else
        d = 0;
        break;
    end
    for i = (k+1):n
        c2 = 0;
        for j = 1:(k-1)
            c2 = c2 + (A(i,j))*(A(k,j));
        end
        A(i,k) = (A(i,k) - c2)/A(k,k);
    end
end
for k = 1:n
    L(k+((k-1)*(2*n-k))/2) = A(k,k);
    for i = k+1:n
    L(i+((k-1)*(2*n-k))/2) = A(i,k);
    
    end
end
end
%=======================================================================================
function X = spddd(L,B)

[n,r] = size(B);

for k = 1:r
    B(1,k) = B(1,k)/L(1);
    for i = 2:n 
        c3 = 0;
	    for j = 1:i-1
	        c3 = c3 + L(i+((j-1)*(2*n-j))/2)*B(j,k);
        end		
        B(i,k) = (B(i,k) - c3)/L(i+((i-1)*(2*n-i))/2);
    end
end

for k = 1:r
    B(n,k) = B(n,k)/(L(((n+1)*n)/2));
    for i = 1:(n-1)
        c4 = 0;
        for j = 1:i
            c4 = c4 + L(((n-i)+(((n-i)-1)*(n+i))/2)+j)*B((n-i+j),k);
        end
        B((n-i),k) = (B((n-i),k) - c4)/L((n-i)+((n-i-1)*(n+i))/2);
    end
end
X = B;
end
%=======================================================================================
function X = ltri(A,B)
[n,r] = size(B);
for k = 1:r
    B(1,k) = B(1,k)/A(1,1);
    for i = 2:n 
        B(i,k) = (B(i,k) - A(i,1:i-1)*B(1:i-1,k))/A(i,i);
    end
end

X = B;

end
%=======================================================================================
function X = utri(A,B)
[n,r] = size(B);
for k = 1:r
    B(n,k) = B(n,k)/A(n,n);
    for i = 1:(n-1)
	    B((n-i),k) = (B((n-i),k) - A((n-i),(n-i+1):n)*B((n-i+1):n,k))/A((n-i),(n-i));
	end
end

X = B;

end
%=======================================================================================
function X = trid(A,B)
[n,r] = size(B);

    for k = 2:n
        A(k,k-1) = A(k,k-1)/A(k-1,k-1);
        A(k,k) = A(k,k) - A(k,k-1)*A(k-1,k);
    end

    for k = 1:r
        for i = 2:n
	        B(i,k) = B(i,k) - A(i,i-1)*B(i-1,k);
	    end
    end	  
    
    for k = 1:r
        B(n,k) = B(n,k)/A(n,n);
        for i = 1:(n-1)
	        B((n-i),k) = (B((n-i),k) - A((n-i),(n-i+1))*B((n-i+1),k))/A((n-i),(n-i));
	    end
    end	  
    
    X = B;
    
end
%=======================================================================================
function X = arroww(A,B)
[n r] = size(B);
[maxx pos] = max(abs(A(1:n-1,n)));
A([pos n],:) = A([n pos],:);
A(:,[pos n]) = A(:,[n pos]);
B([pos n],:) = B([n pos],:);

for i = (1:n-1)
    A(n,n) = A(n,n) - A(i,n)*(A(n,i)/A(i,i));
    B(n,:) = B(n,:) - B(i,:)*(A(n,i)/A(i,i));
end

for k = 1:r
        B(n,k) = B(n,k)/A(n,n);
    for j = 1:(n-1)
        B(n-j,k) = (B(n-j,k) - A(n-j,n)*B(n,k))/A(n-j,n-j);
    end 
end
    B([pos n],:) = B([n pos],:);
    X = B;

end
%==========================================================================
function X = conjuu(A,B)

for k = 1:size(B,2)
     X(:,k) = B(:,k);
     step = 0;
     r=B(:,k)-A*X(:,k);
     p=r;
     rsold=r'*r;
     rsnew=1;
     while sqrt(rsnew)> 0.0001 && step < 1000
         
              Ap=A*p;
              alpha=rsold/(p'*Ap);
              X(:,k)=X(:,k)+alpha*p;
              r=r-alpha*Ap;
              rsnew=r'*r;
              p=r+rsnew/rsold*p;
              rsold=rsnew;

              step = step +1;
     end
end
end

%==========================================================================
function X = pconjuu(A,B)

for k = 1:size(B,2)
     x = B(:,k);
     step = 0;
     P = diag(1./diag(A));
     r=B(:,k)-A*x;
     z=P*r;
     p=z;
     rs=r'*r;
     
     while sqrt(rs)> 0.000001 && step < 10

              Ap=A*p;
              alpha=(p'*r)/(p'*Ap);
              x=x+alpha*p;
              r=r-alpha*Ap;
              z=P*r;
              rs=r'*r;
              beta=(Ap'*z)/(Ap'*p);
              p=z-beta*p;

          step = step +1;
     end
     X(:,k) = x;
end
end
%==========================================================================
function X = ppconjuu(A,B)

for k = 1:size(B,2)
     X(:,k) = B(:,k);
     step = 0;
     r=B(:,k)-A*X(:,k);
     z=(1./diag(A)).*r;
     p=z;
     rs=r'*r;
     
     while sqrt(rs)> 0.000001 && step < 1000

              Ap=A*p;
              alpha=(p'*r)/(p'*Ap);
              X(:,k)=X(:,k)+alpha*p;
              r=r-alpha*Ap;
              z=(1./diag(A)).*r;
              
              beta=(Ap'*z)/(Ap'*p);
              p=z-beta*p;
              rs=r'*r;
          step = step +1;
     end
end
end
%==========================================================================
function X = gausei(A,B)
[n r] = size(B);
X = B;

for k = 1:r
    step = 0;
    while norm(A*X(:,k)-B(:,k),2) > 0.00000001 && step < 30
        
            X(1,k) = (1/A(1,1))*(B(1,k) - A(1,2:n)*X(2:n,k));
        for i = 2:n-1
            X(i,k) = (1/A(i,i))*((B(i,k) - A(i,1:(i-1))*X(1:(i-1),k)) - A(i,(i+1):n)*X((i+1):n,k));
        end
            X(n,k) = (1/A(n,n))*(B(n,k) - A(n,1:(n-1))*X(1:(n-1),k));
            
        step = step + 1;
    end
end

end
%==========================================================================
end