function X = ppconjuu(A,B)

X = B;
for k = 1:size(B,2)

     step = 0;
     r=B(:,k)-A*X(:,k);
     z=(1./diag(A)).*r;
     p=z;
     rs=r'*r;

     while sqrt(rs)> 0.000001 && step < 800 

              Ap=A*p;
              alpha=(p'*r)/(p'*Ap);
              X(:,k)=X(:,k)+alpha*p;
              r=r-alpha*Ap;
              z=(1./diag(A)).*r;
              
              beta=(Ap'*z)/(Ap'*p);
              p=z-beta*p;
              rs=r'*r;

          step = step +1;
     end
end
end
