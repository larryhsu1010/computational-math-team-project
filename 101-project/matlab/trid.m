function X = trid(A,B)
[n,r] = size(B);

    for k = 2:n
        A(k,k-1) = A(k,k-1)/A(k-1,k-1);
        A(k,k) = A(k,k) - A(k,k-1)*A(k-1,k);
    end

    for k = 1:r
        for i = 2:n
	        B(i,k) = B(i,k) - A(i,i-1)*B(i-1,k);
	    end
    end	  
    
    for k = 1:r
        B(n,k) = B(n,k)/A(n,n);
        for i = 1:(n-1)
	        B((n-i),k) = (B((n-i),k) - A((n-i),(n-i+1))*B((n-i+1),k))/A((n-i),(n-i));
	    end
    end	  
    
    X = B;
    
end