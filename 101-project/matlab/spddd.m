function X = spddd(L,B)

[n,r] = size(B);

for k = 1:r
    B(1,k) = B(1,k)/L(1);
    for i = 2:n 
        c3 = 0;
	    for j = 1:i-1
	        c3 = c3 + L(i+((j-1)*(2*n-j))/2)*B(j,k);
        end		
        B(i,k) = (B(i,k) - c3)/L(i+((i-1)*(2*n-i))/2);
    end
end

for k = 1:r
    B(n,k) = B(n,k)/(L(((n+1)*n)/2));
    for i = 1:(n-1)
        c4 = 0;
        for j = 1:i
            c4 = c4 + L(((n-i)+(((n-i)-1)*(n+i))/2)+j)*B((n-i+j),k);
        end
        B((n-i),k) = (B((n-i),k) - c4)/L((n-i)+((n-i-1)*(n+i))/2);
    end
end
X = B;
end