function X = bonus(B)
[n r] = size(B);

for i = 1:n-1
        B(n,:) = B(n,:) + (2^(i-1))*B(n-i,:); 
end

B(n,:) = B(n,:)/(2^(n-1));

for k = 1:r
        B(1,k) = B(1,k) - B(n,k);
    for j = 2:n-1
        B(j,k) = B(j,k) + ones(1,j-1)*B(1:j-1,k) - B(n,k);    
    end
end
X = B;
end
