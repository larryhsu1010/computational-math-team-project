function X = ltri(A,B)
[n,r] = size(B);
for k = 1:r
    B(1,k) = B(1,k)/A(1,1);
    for i = 2:n 
        B(i,k) = (B(i,k) - A(i,1:i-1)*B(1:i-1,k))/A(i,i);
    end
end

X = B;

end