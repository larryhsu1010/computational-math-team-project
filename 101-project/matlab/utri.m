function X = utri(A,B)
[n,r] = size(B);
for k = 1:r
    B(n,k) = B(n,k)/A(n,n);
    for i = 1:(n-1)
	    B((n-i),k) = (B((n-i),k) - A((n-i),(n-i+1):n)*B((n-i+1):n,k))/A((n-i),(n-i));
	end
end

X = B;

end