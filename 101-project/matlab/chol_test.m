function X = chol_test(A,B)
	[L,d]=cholll(A);
	X=spddd(L,B);
end

function [L d]= cholll(A)
[m n] = size(A);
d = 1;
for k = 1:n
    c1 = 0;
    for j = 1:(k-1)
    c1 = c1 + (A(k,j))^2;     % L(k+((j-1)*(2*n-j))/2) = L(k,j)
    end
    c1 = A(k,k)-c1;
    if c1 >= 0
    A(k,k) = c1^(1/2);        % L((((2*n-k+2)*(k-1))/2)+1) = L(k,k)
    else
        d = 0;
        break;
    end
    for i = (k+1):n
        c2 = 0;
        for j = 1:(k-1)
            c2 = c2 + (A(i,j))*(A(k,j));
        end
        A(i,k) = (A(i,k) - c2)/A(k,k);
    end
end
for k = 1:n
    L(k+((k-1)*(2*n-k))/2) = A(k,k);
    for i = k+1:n
    L(i+((k-1)*(2*n-k))/2) = A(i,k);
    
    end
end
end

function X = spddd(L,B)

[n,r] = size(B);

for k = 1:r
    B(1,k) = B(1,k)/L(1);
    for i = 2:n 
        c3 = 0;
	    for j = 1:i-1
	        c3 = c3 + L(i+((j-1)*(2*n-j))/2)*B(j,k);
        end		
        B(i,k) = (B(i,k) - c3)/L(i+((i-1)*(2*n-i))/2);
    end
end

for k = 1:r
    B(n,k) = B(n,k)/(L(((n+1)*n)/2));
    for i = 1:(n-1)
        c4 = 0;
        for j = 1:i
            c4 = c4 + L(((n-i)+(((n-i)-1)*(n+i))/2)+j)*B((n-i+j),k);
        end
        B((n-i),k) = (B((n-i),k) - c4)/L((n-i)+((n-i-1)*(n+i))/2);
    end
end
X = B;
end
