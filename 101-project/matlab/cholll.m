function [L d]= cholll(A)
[m n] = size(A);
d = 1;
for k = 1:n
    c1 = 0;
    for j = 1:(k-1)
    c1 = c1 + (A(k,j))^2;     % L(k+((j-1)*(2*n-j))/2) = L(k,j)
    end
    c1 = A(k,k)-c1;
    if c1 >= 0
    A(k,k) = c1^(1/2);        % L((((2*n-k+2)*(k-1))/2)+1) = L(k,k)
    else
        d = 0;
        break;
    end
    for i = (k+1):n
        c2 = 0;
        for j = 1:(k-1)
            c2 = c2 + (A(i,j))*(A(k,j));
        end
        A(i,k) = (A(i,k) - c2)/A(k,k);
    end
end
for k = 1:n
    L(k+((k-1)*(2*n-k))/2) = A(k,k);
    for i = k+1:n
    L(i+((k-1)*(2*n-k))/2) = A(i,k);
    
    end
end
end