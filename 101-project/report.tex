
\documentclass[a4paper]{article}

\usepackage{xeCJK}
\setCJKmainfont[BoldFont=simhei.ttf,ItalicFont=simkai.ttf]{simsun.ttc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}

\usepackage{fontspec}
\newfontfamily\listingsfont{consola.ttf} 

\usepackage{listings}
% \usepackage{courier}
 \lstset{
         basicstyle=\footnotesize\listingsfont, % Standardschrift
         %numbers=left,               % Ort der Zeilennummern
         numberstyle=\tiny,          % Stil der Zeilennummern
         %stepnumber=2,               % Abstand zwischen den Zeilennummern
         numbersep=5pt,              % Abstand der Nummern zum Text
         tabsize=2,                  % Groesse von Tabs
         extendedchars=true,         %
         breaklines=true,            % Zeilen werden Umgebrochen
         keywordstyle=\color{red},
    		frame=b,         
 %        keywordstyle=[1]\textbf,    % Stil der Keywords
 %        keywordstyle=[2]\textbf,    %
 %        keywordstyle=[3]\textbf,    %
 %        keywordstyle=[4]\textbf,   \sqrt{\sqrt{}} %
         stringstyle=\color{white}\ttfamily, % Farbe der String
         showspaces=false,           % Leerzeichen anzeigen ?
         showtabs=false,             % Tabs anzeigen ?
         xleftmargin=17pt,
         framexleftmargin=17pt,
         framexrightmargin=5pt,
         framexbottommargin=4pt,
         %backgroundcolor=\color{lightgray},
         showstringspaces=false      % Leerzeichen in Strings anzeigen ?        
 }
 \lstloadlanguages{% Check Dokumentation for further languages ...
         %[Visual]Basic
         %Pascal
         %C
         %C++
         %XML
         %HTML
         %Java
         MATLAB
 }
    %\DeclareCaptionFont{blue}{\color{blue}} 

  %\captionsetup[lstlisting]{singlelinecheck=false, labelfont={blue}, textfont={blue}}
\usepackage{color}
\usepackage{caption}
\captionsetup[lstlisting]{singlelinecheck=false, margin=0pt, font={bf,footnotesize}}
\captionsetup[figure]{singlelinecheck=false, margin=0pt, font={bf,footnotesize}}

\usepackage{graphicx}

\usepackage{hyperref}
\hypersetup{colorlinks=true}

\begin{document}

\title{Introduction to Computational Mathematics, Team Project Report}
\date{\today}
\author{Group 21}
\maketitle
\tableofcontents
\pagebreak

\section{Decision Making in the Final Program}
\label{sec:decision}
\subsection{Variables in the Main Program}
In addition to the two major components of the linear system, namely $A$ and
$B$, the following variables are extracted from the structure-type input
\texttt{Lsystem} for more convenient referencing:
\begin{description}
	\item[\texttt{type}]{
		The zero entry distribution type in $A$ as described in project
		specification.  Possible values are integers from 0 to 6.
	}
	\item[\texttt{sprs}]{
		Indicates whether $A$ is a sparse $n$ by $n$ matrix, that is, having
		$\mathcal{O}(n)$ entries known as zeroes in advance.  Possible values
		are 1 (sparse) or 0 (dense).
	}
	\item[\texttt{symm}]{
		Specifies whether $A$ is a symmetric $n$ by $n$ matrix.  Possible
		values are 1 ($A$ is symmetric) or 0 (unknown).
	}
	\item[\texttt{dd}]{
		Specifies whether $A$ is an $n$ by $n$ matrix diagonally dominant by
		row, namely 
		\[
			\left\vert A_{ii} \right\vert \geq \sum^{n}_{\substack{j=1 \\j \neq i}}
			\left\vert A_{ij} \right\vert \text{,}
		\] or by column: 
		\[
			\left\vert A_{ii} \right\vert \geq \sum^{n}_{\substack{j=1 \\j \neq i}}
			\left\vert A_{ji} \right\vert \text{.}
		\] (cf. \cite[chapter 5, page 142]{bib:text}).  Possible values are
	1 ($A$ is symmetric) or 0 (unknown).  }
\end{description}
\subsection{Justification to the Selection Algorithm}
\begin{enumerate}
	\item{
		For cases where \texttt{type} $=1$ or $2$, that is when $A$ is
		lower or upper triangular, a straight-forward forward or backward
		substitution (cf. \ref{sec:ltri} and \ref{sec:utri}) operates
		on $B$ (while retrieving without modifying entries of $A$) with
		predictable (and reasonably efficient) $\mathcal{O}(n^2)$ flops;
		Thus the memory consumption is no more than those required by
		storing $A$ and $B$, and iterative methods offer limited further
		advantage in terms of efficiency.
	}
	\item{
		When A is tridiagonal (\texttt{type} $=3$), Thomas' algorithm (cf.
		\cite[chapter 5, section 5.6]{bib:text}) provides with an
		outstanding $\mathcal{O}(n)$ time complexity, and although $A$ must
		be modified unlike upper-or-lower-triangular cases, a simple trick
		can be conceived to store and replace the computation process of
		$LU$ decomposition in the original $3$ diagonal and sub-diagonal
		vectors of $A$ (cf. \ref{sec:trid}), thus saving additional memory.
	}
	\item{
		As for the 5-diagonal sparse matrix (\texttt{type} $=4$) the team
		has yet to find an algorithm as efficient and resource-saving as
		Thomas' algorithm.  However, the preconditioned conjugate gradient
		method (cf. \ref{sec:ppconjuu}) is functional and suffient for
		solution to problem 6.
	}
	\item{
		A special permutation technique, described by the instructor during
		lecture, is implemented to circumvent the special sparse matrix
		(\texttt{type} $=5$; cf. \ref{sec:arroww}).  The time complexity is
		$\mathcal{O}(n)$, and thanks to flexibility of MATLAB when dealing
		with sparse matrices, memory usage does not rise despite $A$ is
		modified, similar to the tridiagonal case.
	}
	\item{
		The bonus problem (\texttt{type} $=6$) is solved by first a
		(ad hoc by deduction) lower trianglar elimination (with time
		complexity $\mathcal{O}(n)$) and $1$ single-column backward
		substitution ($\mathcal{O}(n)$, also simplified by prior
		deduction). No additional memory consumption is required (cf.
		\ref{sec:bonus}).  The solution is appropriate in itself, thus
		further optimization is of limited value.
	}
	\item{
		The selection process for any general (no zero entry prescribed;
		\texttt{type}$=0$) matrix $A$ is much more complicated:
		\begin{itemize}
			\item{
				If $A$ is diagonally dominant (\texttt{dd}$=1$), the
				conjugate gradient method with the diagonal of $A^TA$ as
				preconditioner (cf. \ref{sec:ppconjuu}) has proven, with
				appropriate termination conditions of steps and error
				preassigned, to be the most efficient among iterative
				methods.  The algorithm operates heavily on $5$ more $n$ by
				$r$ real matrices (\texttt{r}, \texttt{z}, \texttt{p},
				\texttt{Ap}, \texttt{X}, cf. \ref{sec:ppconjuu}) of the
				same sizes as $B$ (every entry is touched), but does not
				require modifying $A$.  Hence considerable memory
				consumption is expected, though catastrophic breakdown will
				not occur.
			}
			\item{
				When \texttt{dd} is unknown ($=0$), other criteria are
				taken into consideration first:
				\begin{itemize}
					\item{
						If $A$ is dense (\texttt{sprs$=0$}), the necessary
						composition of $A^TA$ in the PCG method
						($\mathcal{O}(n^3)$, cf. \ref{sec:ppconjuu}) is
						overly time-consuming compared with Cholesky
						decomposition ($\mathcal{O}(n^3/3)$, cf.
						\ref{sec:cholll}), or even classical Gaussian
						elimination ($LU$ with partial pivoting,
						$\mathcal{O}(\frac{2}{3}n^3)$, cf. \ref{sec:plu}).
						Since the task of checking positive-definiteness of
						$A$ is a non-trivial task (without, of course,
						utilizing builtin functions in MATLAB to compute
						eigenvalues) and checking $A=A^T$ is, nevertheless,
						an easy, $\mathcal{O}(n^2)$ precedure, the final
						selection algorithm is to attempt first Cholesky
						factorization (which is equivalent to $A$ being
						SPD, cf. \ref{sec:cholll} and \ref{sec:spddd})
						whenever $A$ is known to be symmetric (either
						\texttt{symm}$=1$ or by manual check), and treat
						Gaussian elimination (cf.  \ref{sec:plu}) as the
						final resort.  As $A$ is stored in dense format,
						both algorithms will not result in considerable
						memory surge when modifying $A$.
					}
					\item{
						If $A$ is stored in sparse format, the cost of
						acquiring $A^TA$ is less significant
						($\mathcal{O}(n^2)$), and although theoretically
						the computational complexity of Cholesky and LU
						decomposition (cf. \ref{sec:cholll} and
						\ref{sec:plu}) reduces, modifying every entries in
						$A$ will pose severe issues to memory capacity.  By
						using symmetric reverse Cuthill-McKee permutation
						(cf. \cite{bib:MC}) available as the MATLAB function
						\texttt{symrcmm} (cf. \ref{sec:symrcmm1}), the
						problem is reduced to a probably diagonally
						dominant one, with which \texttt{ppconjuu} (cf.
						\ref{sec:ppconjuu}) deals exceptionally well.
					}
				\end{itemize}
			}
		\end{itemize}
	}
\end{enumerate}
\subsection{Summary: A Flow Chart}
\begin{center}
	\includegraphics[width=1.1\textwidth]{flowchart.eps}
	\captionof{figure}{
		A flow chart illustrating the final decision process in the
		main function \texttt{func\_your}.
	}
	\label{fig:flow}
\end{center}
\section{Analysis of Subroutines}
\label{sec:subrou}
\subsection{plu}
\label{sec:plu}
\begin{description}
	\item[Input]{
		$A \in \mathbb{R}^{n \times n}$ $n$ by $n$ matrix, $B \in
		\mathbb{R}^{n \times r}$ $n$ by $r$ matrix
	}
	\item[Output]{$X \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Description]{
			The classical Gaussian LU factorization, as described in
			\cite[section 5.3, page 135]{bib:text}, with partial pivoting
			prefering the largest element (\cite[section 5.3, page
			135]{bib:text}).  The computed entries in L and U are directly
			stored in modified $A$ to save memory.
		}
\end{description}
\lstinputlisting[caption=plu.m]{matlab/plu.m}
\subsection{cholll}
\label{sec:cholll}
\begin{description}
	\item[Input]{$A \in \mathbb{R}^{n \times n}$ $n$ by $n$ matrix}
	\item[Output]{
		$L \in \mathbb{R}^{\frac{n(n-1)}{2}}$ entries of an $n$ by $n$
		upper triangular matrix with positive diagonal entries, stored in
		vector form, $d$ boolean integer
	}
	\item[Description]{
			The Cholesky factorization (\cite[page 143]{bib:text}).  The
			output records only the upper-diagonal entries, thus reducing
			resource requirement.  If negative numbers are encountered in
			the process of calculating square root, break the subroutine
			immediately and return \texttt{d} $=0$ as flag.
		}
\end{description}
\lstinputlisting[caption=cholll.m]{matlab/cholll.m}
\subsection{spddd}
\label{sec:spddd}
\begin{description}
	\item[Input]{
		$L \in \mathbb{R}^{\frac{n(n-1)}{2}}$ entries of an $n$ by $n$
		upper triangular matrix with positive diagonal entries, stored in
		vector form, $B \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix
	}
	\item[Output]{$X \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Description]{
			Subsequent subroutine upon successful invocation of
			\texttt{cholll}.  Solve the equivalent system $L^TLX=L^TB$
			using forward and backward substitutions (cf. \ref{sec:ltri}
			and \ref{sec:utri}), respectively.
		}
\end{description}
\lstinputlisting[caption=spddd.m]{matlab/spddd.m}
\subsection{ltri}
\label{sec:ltri}
\begin{description}
	\item[Input]{
		$A \in \mathbb{R}^{n \times n}$ $n$ by $n$ matrix, $B \in
		\mathbb{R}^{n \times r}$ $n$ by $r$ matrix
	}
	\item[Output]{$X \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Description]{
			Staightforward forward substitution (\cite[(5.9)]{bib:text}).
		}
\end{description}
\lstinputlisting[caption=ltri.m]{matlab/ltri.m}
\subsection{utri}
\label{sec:utri}
\begin{description}
	\item[Input]{
		$A \in \mathbb{R}^{n \times n}$ $n$ by $n$ matrix, $B \in
		\mathbb{R}^{n \times r}$ $n$ by $r$ matrix
	}
	\item[Output]{$X \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Description]{
			Staightforward backward substitution (\cite[(5.10)]{bib:text}).
		}
\end{description}
\lstinputlisting[caption=utri.m]{matlab/utri.m}
\subsection{trid}
\label{sec:trid}
\begin{description}
	\item[Input]{
		$A \in \mathbb{R}^{n \times n}$ $n$ by $n$ matrix, $B \in
		\mathbb{R}^{n \times r}$ $n$ by $r$ matrix
	}
	\item[Output]{$X \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Description]{
			Direct implementation of Thomas' algorithm (described in
			\cite[section, 5.6]{bib:text}).
		}
\end{description}
\lstinputlisting[caption=trid.m]{matlab/trid.m}
\subsection{arroww}
\label{sec:arroww}
\begin{description}
	\item[Input]{
		$A \in \mathbb{R}^{n \times n}$ $n$ by $n$ matrix, $B \in
		\mathbb{R}^{n \times r}$ $n$ by $r$ matrix
	}
	\item[Output]{$X \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Description]{
			The special permutation technique to move the completely
			non-zero row and column to bottom and far right respectively is to
			migitate unnecessary fill-ins that would occur by ordinary
			Gaussian elimination (\texttt{plu}), which saves memory as well as
			execution time.
		}
\end{description}
\lstinputlisting[caption=arroww.m]{matlab/arroww.m}
\subsection{ppconjuu}
\label{sec:ppconjuu}
\begin{description}
	\item[Input]{
		$A \in \mathbb{R}^{n \times n}$ $n$ by $n$ matrix, $B \in
		\mathbb{R}^{n \times r}$ $n$ by $r$ matrix
	}
	\item[Output]{$X \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Description]{
			The Conjugate Gradient Method (\cite[(5.64), section
			5.11]{bib:text}) with the diagonal line of $A$ as
			preconditioner.  The justification behind opting for the PCG
			method, instead of Jacobi or Gauss-Seidel, is that all require
			similar convergence criteria, while PCG converges with better
			speed at the cost of acceptable memory excess.
		}
\end{description}
\lstinputlisting[caption=ppconjuu.m]{matlab/ppconjuu.m}
% \subsection{gausei}
% \lstinputlisting[caption=gausei.m]{matlab/gausei.m}
\subsection{symrcmm1}
\label{sec:symrcmm1}
\begin{description}
	\item[Input]{$B \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Output]{$X \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Description]{
			Acquire the permutation instruction provided by symmetric
			reverse Cuthill-McKee permutation (\cite{bib:MC}) available as
			the MATLAB built-in function \texttt{symrcmm} and call
			\texttt{ppconjuu} with permuted $A$, $B$, and $X$.
		}
\lstinputlisting[caption=symrcmm1.m]{matlab/symrcmm1.m}
\end{description}
\subsection{bonus}
\label{sec:bonus}
\begin{description}
	\item[Input]{$B \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Output]{$X \in \mathbb{R}^{n \times r}$ $n$ by $r$ matrix}
	\item[Description]{
			The current attempt fails for $n > 1000$, as the numeric values
			exceeds the capacity of MATLAB.  The present implementation
			differs from standard elimination (\ref{sec:plu}) by calculating
			$X_n$ first by direct solution deduced from mathematical
			induction. Then it obtains the rest of $X_i, i=1 \ldots n-1$ by
			a single-column backward substitution.
		}
\end{description}
\lstinputlisting[caption=bonus.m]{matlab/bonus.m}
\section{Comparison of Adopted Methods}
\label{sec:compare}
\begin{center}
	\begin{tabular}{|c|c|c|c|c|} \hline
	& & & & \\ 
	\textsc{Algorithms}  & \textsc{Specialty}     & \textsc{Efficiency}   & \textsc{Resource} & \textsc{A Modified} \\
	& & & & \\ \hline
	& & & & \\ 
	\textsf{Thomas}      & $A$ tridiagonal & $\mathcal{O}(n)$  & No additional cost & No \\
	& & & & \\ \hline
	& & & & \\ 
	\textsf{Forward Substitute} & $A$ lower-triangular  & $\mathcal{O}(n^2)$  & No additional cost & No \\
	& & & & \\  \hline
	& & & & \\ 
	\textsf{Backward Substitute} & $A$ upper-triangular  & $\mathcal{O}(n^2)$ & No additional cost & No \\ 
	& & & & \\  \hline
	& & & & \\ 
	\textsf{LU with} & Generic & $\mathcal{O}(\frac{2}{3}n^3)$ & Nearly same & Yes \\ 
	\textsf{Partial Pivoting} & (dense) $A$ & & as $A$ if dense & \\ 
	& & & & \\  \hline
	& & & & \\ 
	\textsf{Cholesky} & Dense symmetric & $\mathcal{O}(\frac{1}{3}n^3)$ & Nearly same & Yes \\
	\textsf{Decomposition} & positive-definite $A$ & & as $A$ if dense & \\ 
	& & & & \\  \hline
	& & & & \\ 
	\textsf{Conjugate Gradient} & Sparse symmetric & $\mathcal{O}(4n^2)$  & Additional $5$ times & No if SPD, \\
	\textsf{Preconditioned} & positive-definite $A$; & each step & the memory of $B$; & otherwize\\ 
	\textsf{by Diagonal Entries} & Diagonally dominant $A$ & (total $\leq n$)& no
	rise if A sparse & yes\\ 
	& & & & \\  \hline
	& & & & \\ 
	\textsf{PCG with reverse} & & $\mathcal{O}(n^2)$ & Nearly same & \\ 
	\textsf{McKee}-Cuthill & Generic (sparse) $A$ & to reorder;& as $A$ if sparse; & Yes \\ 
	\textsf{permutation} & & rest as PCG & rest as PCG & \\
	& & & & \\  \hline
		
		
	\end{tabular}
	\captionof{figure}{
		Comparison of the adopted algorithms.  The functions
		\texttt{arroww} and \texttt{bonus}, as special cases of the
		Gaussian elimination designed for ad hoc problems, are omitted.
	}
	\label{fig:compare}
\end{center}
\section{Numerical Test Results}
\label{sec:numerical}
\subsection{Summary of the Test}
\begin{description}
	\item[Hardware infrastructure]{
		blade3 workstation of the Math Department (blade3.math.ntu.edu.tw).
	}
	\item[Test data]{ \hfill
	\begin{center}
		\includegraphics[width=0.9\textwidth]{01.png}
		\captionof{figure}{
			The test cases selected for $A$ (\texttt{B=ones(length(A),1)}).
			Number 1, 2, 4, 7 are built-in MATLAB functions, while 3, 5, 6,
			8 are acquired from the Florida Sparse Matrix
			Collection \cite{bib:FL}.
		}
		\label{fig:data}
	\end{center}
	}
\end{description}
\subsection{Comparison Chart}
\begin{center}
		\includegraphics[width=0.9\textwidth]{02.png}
		\captionof{figure}{
			The test data in seconds. \texttt{e} is the preassigned error
			for the subroutine \texttt{ppconjuu}.
		}
		\label{fig:chart}
\end{center}
\section{Contribution of Each Team Member}
\begin{description}
		\item[呂勇賢, r01221027]{
				\hfill 
				\begin{itemize}
					\item{Composition of all MATLAB source codes.}
					\item{Debugging the program against assigned problems.}
					\item{Draft for Section \ref{sec:subrou}.}
				\end{itemize}
			}
		\item[徐謙, b97201010]{
				\hfill 
				\begin{itemize}
					\item{Draft for Section \ref{sec:decision}.}
					\item{Numerically testing the code with private data.}
					\item{Draft for Section \ref{sec:numerical} (not
					typeset).}
					\item{Compilation of the final report in \LaTeX.}
				\end{itemize}
			}
		\item[方致凱, b97201041]{
				\hfill 
				\begin{itemize}
					\item{Draft for Section \ref{sec:compare}.}
					\item{Coordination.}
				\end{itemize}
			}
\end{description}

\begin{thebibliography}{9}

	\bibitem[QSG]{bib:text}
	Quarteroni, A., \& Saleri, F., \& Gervasio, P.,
	\emph{Scientific Computing with MATLAB and Octave}.
	Springer,
	3nd Edition,
	2010.
	\bibitem[MC]{bib:MC}
	E. Cuthill, \& J. McKee.
	\emph{Reducing the bandwidth of sparse symmetric matrices}.
	Proc. 24th Nat. Conf. ACM,
	pages 157–172,
	1969.
	\bibitem[FL]{bib:FL}
	T. A. Davis \& Y. Hu,
	\emph{The University of Florida Sparse Matrix Collection}.
	ACM Transactions on Mathematical Software,
	\url{http://www.cise.ufl.edu/research/sparse/matrices}.
\end{thebibliography}
\end{document}
